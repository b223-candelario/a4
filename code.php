<?php 

class Building{

	protected $name;
	protected $floor;
	protected $address;

	public function __construct($name, $floor, $address){
		$this->name = $name;
		$this->floor = $floor;
		$this->address = $address;
	}

	public function getName(){
		return $this->name;
	}

	public function getFloorNo(){
		return $this->floor;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setName($name){
		if(strlen($name) !== 0){
			$this->name = $name;
		}
	}
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');


class Condominium extends Building{
	
}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');

?>