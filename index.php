<?php require_once './code.php' ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03: Classes and Objects</title>
</head>
<body>
	<h1>Building</h1>

	<p><?php echo "The name of the building is " . $building->getName() . "."; ?></p>

	<p><?php echo "The " . $building->getName() . " has " . $building->getFloorNo() . " floors."; ?></p>

	<p><?php echo "The " . $building->getName() . " is located at " . $building->getAddress() . "."; ?></p>

	<?php $building->setName('Caswynn Complex') ?>

	<p><?php echo 'The name of the building has been changed to ' . $building->getName() . "."; ?></p>



	<h1>Condominium</h1>

	<p><?php echo "The name of the condominium is " . $condominium->getName() . "."; ?></p>

	<p><?php echo "The " . $condominium->getName() . " has " . $condominium->getFloorNo() . " floors."; ?></p>

	<p><?php echo "The " . $condominium->getName() . " is located at " . $condominium->getAddress() . "."; ?></p>

	<?php $condominium->setName('Enzo Tower') ?>

	<p><?php echo 'The name of the condominium has been changed to ' . $condominium->getName() . "."; ?></p>
</body>
</html>